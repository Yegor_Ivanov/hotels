
typealias Callback<T> = (T?) -> ()
typealias NonOptionalCallback<T> = (T) -> ()
let kEmptyCallback: (Any) -> () = {_ in}
typealias Pingback = () -> ()

typealias ImageCallback = Callback<UIImage>
typealias HotelCallback = NonOptionalCallback<Hotel>
typealias HotelsCallback = NonOptionalCallback<[Hotel]>
typealias Failure = Pingback

import UIKit

extension UIView {
	var boundsCenter: CGPoint {
		return CGPoint(x: bounds.width / 2, y: bounds.height / 2)
	}
}

extension UIView {
	func addSpinner() -> UIView {
		let backgroundView = UIView(frame: self.bounds)
		backgroundView.backgroundColor = .white
		addSubview(backgroundView)
		backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
		backgroundView.addSubview(spinner)
		spinner.center = CGPoint(x: backgroundView.bounds.midX, y: backgroundView.bounds.midY)
		spinner.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
		spinner.startAnimating()
		return backgroundView
	}
}
