
import UIKit

enum ImageError: Error {
	case noImage
}

class HotelDetailsPresenter: DetailsPresenter, DetailsViewHandler {
	var interactor: DetailsInteractor!
	weak var view: HotelDetailsViewController!
	var router: Router!
	
	func viewDidLoad() {
		interactor.viewDidLoad()
	}
	
	func showDetails(for hotel: Hotel) {
		view.showMap(with: hotel.name, lon: hotel.lon ?? 0, lat: hotel.lat ?? 0)
		view.nameLabel.text = hotel.name
		view.addressLabel.text = hotel.address
		view.ratingView.setRating(Int(hotel.stars))
		let count = hotel.suitesAvailability?.count ?? 0
		let word = count == 1 ? "suit" : "suits"
		view.suitesLabel.text = "\(count) \(word) available"
		view.update()
	}
	
	func showImage(_ image: UIImage) {
		prepareImage(image) { image in
			self.view.showImage(image)
		}
	}
	
	func showNoImage() {
		view.showNoImage()
	}
	
	func showFailure(_ error: Error) {
		view.showNoImage()
		let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		view.present(alert, animated: true, completion: nil)
	}
	
	private func prepareImage(_ image: UIImage, callback: @escaping (UIImage)->()) {
		DispatchQueue.global().async {
			let format = UIGraphicsImageRendererFormat()
			format.scale = 1.0
			let size = CGSize(width: image.size.width - 2, height: image.size.height - 2)
			let image = UIGraphicsImageRenderer(size: size, format: format).image { context in
				image.draw(at: CGPoint(x: -1, y: -1))
			}
			DispatchQueue.main.async {
				callback(image)
			}
		}
	}
}
