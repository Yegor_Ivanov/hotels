
import UIKit

class HotelDetails: DetailsInteractor {
	var dataSource: DataSource!
	weak var presenter: DetailsPresenter?
	var hotel: Hotel!
	
	func viewDidLoad() {
		presenter?.showDetails(for: hotel)
		dataSource.getHotelDetails(with: hotel.id, callback: { [weak self] hotel in
			self?.hotel = hotel
			self?.getImage()
			self?.presenter?.showDetails(for: hotel)
		}) { [weak self] error in
			self?.presenter?.showFailure(error)
		}
	}
	
	private func getImage() {
		guard let imageID = hotel.imageID else {
			presenter?.showNoImage()
			return
		}
		dataSource.getImage(with: imageID, callback: { [weak self] image in
			guard let image = image else {
				self?.presenter?.showNoImage()
				return
			}
			self?.presenter?.showImage(image)
		}) { [weak self] in
			self?.presenter?.showNoImage()
		}
	}
}
