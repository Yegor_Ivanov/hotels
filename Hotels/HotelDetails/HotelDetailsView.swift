
import UIKit
import MapKit

class HotelDetailsViewController: UITableViewController, DetailsView {
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var spinner: UIActivityIndicatorView!
	@IBOutlet weak var noImageLabel: UILabel!
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var ratingView: StarsView!
	@IBOutlet weak var suitesLabel: UILabel!
	@IBOutlet weak var openMapCell: UITableViewCell!
	private var mapItem: MKMapItem?
	
	var handler: DetailsViewHandler!
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		showSpinner()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 60
		handler.viewDidLoad()
	}
	
	func update() {
		tableView.reloadData()
	}
	
	func showImage(_ image: UIImage) {
		hideSpinner()
		imageView.image = image
	}
	
	func showNoImage() {
		imageView.image = nil
		noImageLabel.isHidden = false
		spinner.isHidden = true
	}
	
	private func hideSpinner() {
		spinner.isHidden = true
	}
	
	private func showSpinner() {
		noImageLabel.isHidden = true
		spinner.isHidden = false
		spinner.startAnimating()
	}
	
	func updateHeights() {
		tableView.setNeedsLayout()
		tableView.layoutIfNeeded()
	}
	
	func showMap(with name: String, lon: Double, lat: Double) {
		let coordinates = CLLocationCoordinate2DMake(lat, lon)
		let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
		mapItem = MKMapItem(placemark: placemark)
		mapItem?.name = name
		mapView.addAnnotation(mapItem!.placemark)
		mapView.showAnnotations([mapItem!.placemark], animated: false)
	}
	
	private func openMap() {
		mapItem?.openInMaps(launchOptions: nil)
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if let cell = tableView.cellForRow(at: indexPath), cell == openMapCell {
			openMap()
		}
	}
}
