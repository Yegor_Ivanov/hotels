
import Foundation
import Alamofire

class Hotels: DataSource {
	func getHotels(_ callback: @escaping ([Hotel]) -> (), failure: @escaping (Error) -> ()) {
		let url = "https://raw.githubusercontent.com/iMofas/ios-android-test/master/0777.json"
		request(with: url, callback: { [unowned self] data in
			do { callback(try self.parseHotelsList(data)) }
			catch { failure(error) }
		}, failure: failure)
	}
	
	func getHotelDetails(with hotelID: Int, callback: @escaping (Hotel) -> (), failure: @escaping (Error) -> ()) {
		let url = "https://raw.githubusercontent.com/iMofas/ios-android-test/master/\(hotelID).json"
		request(with: url, callback: { [unowned self] data in
			do { callback(try self.parseHotel(data)) }
			catch { failure(error) }
		}, failure: failure)
	}
	
	func getImage(with imageID: String, callback: @escaping (UIImage?) -> (), failure: @escaping () -> ()) {
		let url = "https://github.com/iMofas/ios-android-test/raw/master/\(imageID)"
		request(with: url, callback: { data in
			callback(UIImage(data: data))
		}) { _ in failure() }
	}
	
	private func request(with url: String, callback: @escaping  (Data)->(), failure: @escaping (Error) -> ()) {
		Alamofire.request(url).responseData { response in
			switch response.result {
			case .success(let data):
				callback(data)
			case .failure(let error):
				failure(error)
			}
		}
	}
	
	private func parseHotel(_ data: Data) throws -> Hotel {
		let decoder = JSONDecoder()
		return try decoder.decode(Hotel.self, from: data)
	}
	
	private func parseHotelsList(_ data: Data) throws -> [Hotel] {
		let decoder = JSONDecoder()
		return try decoder.decode([Hotel].self, from: data)
	}
}
