
import UIKit

protocol Router {
	func showList()
	func showHotelDetails(for hotel: Hotel)
}

protocol Factory {
	func buildListModule() -> Module<ListInteractor, ListPresenter, ListView>
	func buildDetailsModule() -> Module<DetailsInteractor, DetailsViewHandler, DetailsView>
}

// Data Source

protocol DataSource {
	func getHotels(_ callback: @escaping ([Hotel]) -> (), failure: @escaping (Error) -> ())
	func getHotelDetails(with hotelID: Int, callback: @escaping (Hotel) -> (), failure: @escaping (Error) -> ())
	func getImage(with imageID: String, callback: @escaping (UIImage?) -> (), failure: @escaping () -> ())
}

// Hotels List

protocol ListInteractor: class {
	var dataSource: DataSource! { set get }
	weak var presenter: ListPresenter? { get set }
	func getHotels()
	func sortByDistance()
	func sortBySuitsAvailability()
}

enum HotelsSorting { case undefined, byDistance, bySuitsAvailability }

protocol ListPresenter: class {
	func showHotelsList(_ hotels: [Hotel], sorting: HotelsSorting)
	func showFailure(_ error: Error)
}

protocol ListViewHandler: class {
	var router: Router! { get set }
	var interactor: ListInteractor! { get set }
	func viewDidLoad()
	func sortByDistancePressed()
	func sortBySuitsAvailabilityPressed()
}

protocol ListView: class {
	func showList(_ cellModels: [CellModel])
}

// Hotel Details

protocol DetailsInteractor: class {
	weak var presenter: DetailsPresenter? { get set }
	func viewDidLoad()
}

protocol DetailsPresenter: class {
	func showDetails(for hotel: Hotel)
	func showFailure(_ error: Error)
	func showNoImage()
	func showImage(_ image: UIImage)
}

protocol DetailsViewHandler: class {
	var router: Router! { get set }
	func viewDidLoad()
}

protocol DetailsView: class {
	var handler: DetailsViewHandler! { get set }
	func update()
	func showNoImage()
	func showImage(_ image: UIImage)
	func showMap(with name: String, lon: Double, lat: Double)
}
