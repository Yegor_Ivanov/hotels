
import UIKit

struct Module<Interactor, Presenter, View> {
	var interactor: Interactor?
	var presenter: Presenter?
	var view: View?
}

class AppFactory {
	private let storyboardFileName = "Main"
	private let rootControllerID = "RootNavigationController"
	
	private var router: AppRouter?
	private var window: UIWindow
	private var storyboard: UIStoryboard?
	private var dataSource = Hotels()
	
	init(with window: UIWindow) {
		self.window = window
	}
	
	func setupInitialState() {
		storyboard = UIStoryboard(name: storyboardFileName, bundle: nil)
		router = AppRouter(with: window, factory: self, rootController: rootController())
		router?.start()
	}
	
	private func rootController() -> UINavigationController {
		let controller = storyboard?.instantiateViewController(withIdentifier: rootControllerID)
		return controller as! UINavigationController
	}
	
	func buildListModule() -> Module<ListInteractor, ListPresenter, HotelsListViewController> {
		let interactor = HotelsList()
		let presenter = HotelsListPresenter()
		let view = viewController(ofType: HotelsListViewController.self)
		
		interactor.presenter = presenter
		interactor.dataSource = dataSource
		presenter.interactor = interactor
		presenter.router = router
		view?.handler = presenter
		presenter.view = view
		
		return Module(interactor: interactor, presenter: presenter, view: view)
	}
	
	func buildDetailsModule(for hotel: Hotel) -> Module<DetailsInteractor, DetailsViewHandler, HotelDetailsViewController> {
		let interactor = HotelDetails()
		interactor.hotel = hotel
		let presenter = HotelDetailsPresenter()
		interactor.dataSource = dataSource
		interactor.presenter = presenter
		presenter.interactor = interactor
		presenter.router = router
		let view = viewController(ofType: HotelDetailsViewController.self)
		presenter.view = view
		view?.handler = presenter
		return Module(interactor: nil, presenter: nil, view: view)
	}
	
	private func viewController<T:UIViewController>(ofType: T.Type) -> T? {
		return storyboard?.instantiateViewController(withIdentifier: String(describing: T.self)) as? T
	}
}
