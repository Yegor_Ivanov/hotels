
import UIKit

class AppRouter: Router {
	private var window: UIWindow
	private var factory: AppFactory
	private var rootController: UINavigationController
	
	init(with window: UIWindow, factory: AppFactory, rootController: UINavigationController) {
		self.window = window
		self.factory = factory
		self.rootController = rootController
	}
	
	func start() {
		window.rootViewController = rootController
		window.makeKeyAndVisible()
		showList()
	}
	
	func showList() {
		let list = factory.buildListModule()
		guard let listView = list.view else {
			assertionFailure("List view is not set")
			return
		}
		rootController.pushViewController(listView, animated: false)
	}
	
	func showHotelDetails(for hotel: Hotel) {
		let details = factory.buildDetailsModule(for: hotel)
		rootController.pushViewController(details.view!, animated: true)
	}
}
