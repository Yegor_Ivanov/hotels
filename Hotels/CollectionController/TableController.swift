
import UIKit

class TableController: NSObject, UITableViewDelegate, UITableViewDataSource {
	@IBOutlet var tableViewOutlet: UITableView?
	var didScrollToEnd: Pingback?
	var tableViewProperty: UITableView?
	
	func setup() {
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 60
	}
	
	var tableView: UITableView {
		guard let tableView = tableViewOutlet ?? tableViewProperty else {
			fatalError("tableview was not set")
		}
		return tableView
	}
	
	func register<T: CellModel>(_ cellModelTypes: [T]) {
		for modelType in cellModelTypes {
			tableView.register(modelType.cellType as? AnyClass, forCellReuseIdentifier: modelType.cellID)
		}
	}
	
	var sections = [SectionModel]()
	
	func add(_ section: SectionModel) {
		sections.append(section)
	}
	
	private func section(for index: Int) -> SectionModel? {
		guard sections.count > index else { return nil }
		return sections[index]
	}
	
	func add(_ cellModels: [CellModel], toSection index: Int) {
		guard tableView.delegate != nil && tableView.dataSource != nil else {
			assertionFailure("tableview's delegates are not set")
			return
		}
		
		guard sections.count > index else {
			sections.append(SectionModel(with: nil, cellModels: [CellModel]()))
			tableView.insertSections(IndexSet(integer:(sections.count - 1)), with: .fade)
			add(cellModels, toSection: index)
			return
		}
		
		let section = self.section(for: index)!
		let oldBound = section.cellModels.count
		section.cellModels.append(contentsOf: cellModels)
		
		var indexes = [IndexPath]()
		for i in oldBound ..< cellModels.count + oldBound {
			indexes.append(IndexPath(item: i, section: index))
		}
		
		tableView.insertRows(at: indexes, with: .fade)
	}
	
	func replace(_ cellModels: [CellModel], inSection index: Int) {
		guard let section = section(for: index) else {
			sections.append(SectionModel(cellModels: []))
			tableView.insertSections(IndexSet(integer: index), with: .automatic)
			replace(cellModels, inSection: index)
			return
		}
		tableView.beginUpdates()
		if section.cellModels.count > 0 {
			let indexes = section.cellModels.enumerated().map { IndexPath(item: $0.offset, section: index) }
			section.cellModels = []
			tableView.deleteRows(at: indexes, with: .fade)
		}
		section.cellModels = cellModels
		let indexes = cellModels.enumerated().map { IndexPath(item: $0.offset, section: index) }
		tableView.insertRows(at: indexes, with: .fade)
		tableView.endUpdates()
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		guard let model = sections[indexPath.section].cellModels[indexPath.item] as? SelectableCellModel else { return }
		guard let cell = tableView.cellForRow(at: indexPath) else { return }
		model.selectionCallback?(cell)
	}
	
	func removeAllCells() {
		sections.removeAll()
		tableView.deleteSections(IndexSet(0..<self.sections.count), with: .fade)
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return sections.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections[section].cellModels.count
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let sectionCellsCount = sections[indexPath.section].cellModels.count
		if indexPath.section == sections.count - 1 && indexPath.item == sectionCellsCount - 1 {
			didScrollToEnd?()
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let model = sections[indexPath.section].cellModels[indexPath.item]
		let cell = tableView.dequeueReusableCell(withIdentifier: model.cellID, for: indexPath) as! ConfigurableCell
		cell.setup(with: model)
		return cell as! UITableViewCell
	}
}
