
import UIKit

// Section Model

class SectionModel {
	var title: String?
	var cellModels: [CellModel]
	init(with title: String? = nil, cellModels: [CellModel]) {
		self.title = title
		self.cellModels = cellModels
	}
}

// Cell Model

protocol CellModel {
	var cellType: ConfigurableCell.Type { get }
	var cellID: String { get }
}

extension CellModel {
	var cellID: String {
		return cellType.cellID
	}
}

protocol SelectableCellModel {
	var selectionCallback: NonOptionalCallback<UITableViewCell>? { get set }
}

// Cell

protocol ConfigurableCell {
	static var cellID: String { get }
	func setup(with genericModel: CellModel)
}

extension ConfigurableCell {
	static var cellID: String {
		return String(describing: self)
	}
}

