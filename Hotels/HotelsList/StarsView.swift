
import UIKit

class StarsView: UIView {
	private let filledStar = #imageLiteral(resourceName: "filledStar")
	private let emptyStar = #imageLiteral(resourceName: "emptyStar")
	private var stars = [UIImageView]()
	private let offset: CGFloat = 2
	
	func setRating(_ rating: Int) {
		stars.forEach { $0.image = emptyStar }
		stars.prefix(upTo: rating).forEach { $0.image = filledStar }
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		(0 ..< 5).forEach { _ in
			let imageView = UIImageView(frame: CGRect(origin: .zero, size: emptyStar.size))
			imageView.contentMode = .center
			stars.append(imageView)
			addSubview(imageView)
		}
		setRating(0)
	}
	
	override var intrinsicContentSize: CGSize {
		return CGSize(width: (emptyStar.size.width + offset) * CGFloat(stars.count), height: emptyStar.size.height)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		for i in 0 ..< stars.count {
			let star = stars[i]
			let width = emptyStar.size.width
			star.center = CGPoint(x: CGFloat(i) * (width + offset) + width / 2, y: bounds.height / 2)
		}
	}
}
