
import UIKit

struct HotelsListCellModel: CellModel, SelectableCellModel {
	var name: String?
	var stars: Double?
	var distanceOrSuits: String?
	var cellType: ConfigurableCell.Type {
		return HotelsListCell.self
	}
	var selectionCallback: ((UITableViewCell) -> ())?
}

class HotelsListCell: UITableViewCell, ConfigurableCell {
	var model: HotelsListCellModel?
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var ratingView: StarsView!
	@IBOutlet weak var distanceOrSuitsLabel: UILabel!
	
	func setup(with genericModel: CellModel) {
		model = genericModel as? HotelsListCellModel
		nameLabel.text = model?.name
		distanceOrSuitsLabel.text = model?.distanceOrSuits
		let rating = Int(model?.stars ?? 0)
		ratingView.setRating(rating)
	}
}
