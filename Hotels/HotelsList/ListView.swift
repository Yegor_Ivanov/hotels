
import UIKit

class HotelsListViewController: UIViewController, ListView {
	@IBOutlet var tableController: TableController!
	var handler: ListViewHandler?
	private weak var spinner: UIView?
	
	func showList(_ cellModels: [CellModel]) {
		spinner?.removeFromSuperview()
		tableController.replace(cellModels, inSection: 0)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableController.setup()
		setupSortingButton()
		handler?.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if tableController.sections.count == 0 {
			spinner = view.addSpinner()
		}
	}
	
	private func setupSortingButton() {
		let button = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortingButtonPressed(_:)))
		navigationItem.rightBarButtonItem = button
	}
	
	@objc private func sortingButtonPressed(_ sender: UIBarButtonItem) {
		let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		alert.addAction(UIAlertAction(title: "Sort by Distance", style: .default) { _ in
			self.handler?.sortByDistancePressed()
		})
		alert.addAction(UIAlertAction(title: "Sort by Suits Availability", style: .default) { _ in
			self.handler?.sortBySuitsAvailabilityPressed()
		})
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		present(alert, animated: true, completion: nil)
	}
}
