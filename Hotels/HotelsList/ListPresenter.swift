
import UIKit

class HotelsListPresenter: ListPresenter, ListViewHandler {
	
	var router: Router!
	var interactor: ListInteractor!
	var view: HotelsListViewController!
	
	func showHotelsList(_ hotels: [Hotel], sorting: HotelsSorting) {
		let cellModels = hotels.map { cellModel(for: $0, sorting: sorting) }
		view.showList(cellModels)
	}
	
	func showFailure(_ error: Error) {
		let alert = UIAlertController(title: "Failed to load", message: error.localizedDescription, preferredStyle: .alert)
		let tryAgain = UIAlertAction(title: "Try Again", style: .default) { action in
			self.interactor?.getHotels()
		}
		alert.addAction(tryAgain)
		view?.present(alert, animated: true, completion: nil)
	}
	
	func cellModel(for hotel: Hotel, sorting: HotelsSorting = .undefined) -> CellModel {
		var model = HotelsListCellModel()
		model.name = hotel.name
		model.stars = hotel.stars
		switch sorting {
		case .byDistance:
			model.distanceOrSuits = "\(hotel.distance) km"
		case .bySuitsAvailability:
			let count = hotel.suitesAvailability?.count ?? 0
			let word = count == 1 ? "suit" : "suits"
			model.distanceOrSuits = "\(count) \(word)"
		default: break
		}
		model.selectionCallback = { _ in
			self.router?.showHotelDetails(for: hotel)
		}
		return model
	}
	
	// view handler
	
	func viewDidLoad() {
		interactor?.getHotels()
	}
	
	func sortByDistancePressed() {
		interactor?.sortByDistance()
	}
	
	func sortBySuitsAvailabilityPressed() {
		interactor?.sortBySuitsAvailability()
	}
}
