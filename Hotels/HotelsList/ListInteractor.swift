
import UIKit

class HotelsList: ListInteractor {
	var dataSource: DataSource!
	weak var presenter: ListPresenter?
	private var hotels = [Hotel]()
	private var currentSorting = HotelsSorting.undefined
	
	func getHotels() {
		dataSource.getHotels({ [weak self] hotels in
			self?.hotels = hotels
			self?.showHotels()
		}) { [weak self] error in
			self?.presenter?.showFailure(error)
		}
	}
	
	private func showHotels() {
		presenter?.showHotelsList(hotels, sorting: currentSorting)
	}
	
	func sortByDistance() {
		currentSorting = .byDistance
		sortHotels()
		showHotels()
	}
	
	func sortBySuitsAvailability() {
		currentSorting = .bySuitsAvailability
		sortHotels()
		showHotels()
	}
	
	private func sortHotels() {
		switch currentSorting {
		case .byDistance:
			hotels = hotels.sorted { $0.distance < $1.distance }
		case .bySuitsAvailability:
			hotels = hotels.sorted { $0.suitesAvailability?.count ?? 0 > $1.suitesAvailability?.count ?? 0 }
		default: break
		}
	}
}
