
import Foundation

struct Hotel: Decodable {
	let id: Int
	let name: String
	let address: String
	let stars: Double
	let distance: Double
	let suitesAvailability: [Int]?
	let imageID: String?
	let lat: Double?
	let lon: Double?
	
	enum CodingKeys: String, CodingKey {
		case id
		case name
		case address
		case stars
		case lat
		case lon
		case imageID = "image"
		case distance
		case suitesAvailability = "suites_availability"
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decode(Int.self, forKey: .id)
		name = try values.decode(String.self, forKey: .name)
		address = try values.decode(String.self, forKey: .address)
		stars = try values.decode(Double.self, forKey: .stars)
		distance = try values.decode(Double.self, forKey: .distance)
		imageID = try values.decodeIfPresent(String.self, forKey: .imageID)
		lat = try values.decodeIfPresent(Double.self, forKey: .lat)
		lon = try values.decodeIfPresent(Double.self, forKey: .lon)
		if let string = try values.decodeIfPresent(String.self, forKey: .suitesAvailability){
			suitesAvailability = string.components(separatedBy: ":").flatMap { Int($0) }
		} else {
			suitesAvailability = nil
		}
	}
}
